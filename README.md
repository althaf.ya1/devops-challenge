## DevOps challenge Project
This is for deploying flask app into Vagrant VM (ubuntu/trusty64) and configure using ansible playbooks

## Getting Started
Check out code by running following command
`git clone https://gitlab.com/althaf.ya1/devops-challenge.git`

`cd devops-challenge`

`vagrant up`

## Workflow
1. Vagrantfile will deploy VM
2. Vagrant guest VM would be configured with ip - `10.10.10.20`
2. Vagrant guest VM will have max CPUs assigned from host
3. Ansible playbook (galaxy) called kickstart.yml (`provisioning/kickstart.yml`) which will initiate the package installation and code deployment part
4. flask-api will be deployed through wsgi service and the same will be configured to run through upstart script to handle auto-start in case of any crash or process kill
5. nginx used for reverse proxy as flask by default will expose to 5000 port which will be redirected through port 80 for outside world
6. enabled sync folder option just a temporary solution to share config/script files between host to vagrant guest VM.
7. However in the realtime scenario, its good to have them mangaed along with app code (eg; infra folder) in SCM which will reduce code handling and sync etc.,
 

# Pre-requisite
1. `ansible > 2.0`
2. `virtualbox > 5.1` # the minimum version I had and did not check prior versions

# Result
1. code checkout happens at /webapps/devops folder and it will have owner/group as `vagrant:www-data` so that both vagrant and nginx will have complete access
2. All the dependencies and pre-requisite packages will be installed using ansible 
3. The app (app name: flask-api) will run as non-previleged user (vagrant)
   you can check the same by running `ps -ef |grep flask-api|grep -v grep` inside vagrant Guest VM
4. since the app startup handled through upstart script with respawn, it will keep an eye on the parent process and will restart in case of crash or process kill
5. provisioned VM will have max available CPUs from hosts
6. Daily logrotate will be in place for flask-api logs (`/webapps/devops/logs/*.log`) and will keep 30 log files for any troubleshooting need.
7. Time zone by default will be UTC so if any other timezone needed we could handle it through ansible using timezone module
8. Last, you can run from host to see if flask-api is deployed and reverse proxied through nginx by running following command
   `curl http://10.10.10.20`

## Author-information
Althaf (DevOps)
